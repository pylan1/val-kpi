from datetime import datetime

from sqlalchemy import (
    Column,
    Integer,
    DateTime,
    ForeignKey,
)
from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import relationship
from val_kpi import l

meta = MetaData(schema='stats')
common_time = datetime.now().isoformat()


class MetaVALKBase(declarative_base(metadata=meta)):
    __abstract__ = True


_exec_time = None


class S3ExecTime(MetaVALKBase):
    __tablename__ = "s3_exec_time"

    id_s3_exec_time: int = Column(Integer, primary_key=True)
    exec_time: datetime = Column(DateTime)

    def __init__(self) -> None:
        super().__init__()

    def set_time(self, dt: datetime):
        self.exec_time = common_time


def get_exec_time():
    global _exec_time
    if not _exec_time:
        l.debug(f"exec_time: {common_time}")
        _exec_time = S3ExecTime()
        _exec_time.set_time(common_time)
    return _exec_time


class VALKBase(MetaVALKBase):
    __abstract__ = True

    @declared_attr
    def id_s3_exec_time(cls):
        return Column(Integer, ForeignKey('s3_exec_time.id_s3_exec_time'))

    @declared_attr
    def exec_time(cls):
        return relationship("S3ExecTime")

    def __init__(self) -> None:
        super().__init__()
        self.exec_time = get_exec_time()

    def update_from_source(self, source: object) -> None:
        pass
