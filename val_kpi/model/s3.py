from datetime import datetime
from sqlalchemy import (
    Column,
    String,
    Integer,
    BigInteger,
    DateTime,
    Boolean,
    ForeignKey,
)
from sqlalchemy.orm import relationship
from val_kpi.model.base import VALKBase, S3ExecTime
from val_kpi.model.base import common_time


class S3User(VALKBase):
    __tablename__ = "s3_user"
    id_s3_user: int = Column(Integer, primary_key=True)
    id_s3_quota_user: int = Column(Integer, ForeignKey('s3_quota.id_s3_quota'))
    id_s3_quota_bucket: int = Column(Integer, ForeignKey('s3_quota.id_s3_quota'))
    id_s3_exec_time: int = Column(Integer, ForeignKey('s3_exec_time.id_s3_exec_time'))

    user_id: str = Column(String)
    display_name: str = Column(String)
    suspended: int = Column(Integer)
    max_buckets: int = Column(Integer)
    system: bool = Column(Boolean)
    admin: bool = Column(Boolean)

    exec_time: S3ExecTime = relationship("S3ExecTime", foreign_keys=[id_s3_exec_time])
    buckets: 'S3Bucket' = relationship("S3Bucket", uselist=True, collection_class=set)
    user_quota: 'S3Quota' = relationship("S3Quota", foreign_keys=[id_s3_quota_user])
    bucket_quota: 'S3Quota' = relationship("S3Quota", foreign_keys=[id_s3_quota_bucket])

    def update_from_source(self, source: object) -> None:
        super().update_from_source(source)
        self.user_id = source['user_id']
        self.display_name = source['display_name']
        self.suspended = source['suspended']
        self.max_buckets = source['max_buckets']
        self.system = True if source['system'].lower() == 'true' else False
        self.admin = True if source['admin'].lower() == 'true' else False


class S3Bucket(VALKBase):
    __tablename__ = "s3_bucket"
    id_s3_bucket: int = Column(Integer, primary_key=True)
    id_s3_user: int = Column(Integer, ForeignKey('s3_user.id_s3_user'))
    id_s3_exec_time: int = Column(Integer, ForeignKey('s3_exec_time.id_s3_exec_time'))
    id_s3_quota: int = Column(Integer, ForeignKey('s3_quota.id_s3_quota'))

    # TODO renommer vers bucket pour conserver la parité avec le modèle S3
    name: str = Column(String)
    num_shards: int = Column(Integer)
    bucket_id: str = Column(String)
    owner: str = Column(String)
    mtime: datetime = Column(DateTime)
    creation_time: datetime = Column(DateTime)

    exec_time: S3ExecTime = relationship("S3ExecTime", foreign_keys=[id_s3_exec_time])
    quota: 'S3Quota' = relationship("S3Quota", foreign_keys=[id_s3_quota])
    usages: 'S3Usage' = relationship("S3Usage", uselist=True, collection_class=set)

    def update_from_source(self, source: object) -> None:
        super().update_from_source(source)
        self.name = source["bucket"]
        self.num_shards = source["num_shards"]
        self.bucket_id = source["id"]
        self.owner = source["owner"]
        self.mtime = source["mtime"]
        self.creation_time = source["creation_time"]


class S3Usage(VALKBase):
    __tablename__ = "s3_usage"
    id_s3_usage: int = Column(Integer, primary_key=True)
    id_s3_bucket: int = Column(Integer, ForeignKey('s3_bucket.id_s3_bucket'))
    id_s3_exec_time: int = Column(Integer, ForeignKey('s3_exec_time.id_s3_exec_time'))

    usage_exec_time: str = Column(String)
    usage_exec_datetime: datetime = Column(DateTime)
    size: int = Column(BigInteger)
    size_actual: int = Column(BigInteger)
    size_utilized: int = Column(BigInteger)
    num_objects: int = Column(Integer)
    exec_time: S3ExecTime = relationship("S3ExecTime", foreign_keys=[id_s3_exec_time])
    bucket: S3ExecTime = relationship("S3Bucket", foreign_keys=[id_s3_bucket])

    def __init__(self) -> None:
        super().__init__()
        self.usage_exec_time = common_time
        self.usage_exec_datetime = common_time

    def update_from_source(self, source: object) -> None:
        super().update_from_source(source)
        self.size = source['size']
        self.size_actual = source['size_actual']
        self.size_utilized = source['size_utilized']
        self.num_objects = source['num_objects']


class S3Quota(VALKBase):
    __tablename__ = "s3_quota"
    id_s3_quota: int = Column(Integer, primary_key=True)
    id_s3_exec_time: int = Column(Integer, ForeignKey('s3_exec_time.id_s3_exec_time'))

    enabled: bool = Column(Boolean)
    check_on_raw: bool = Column(Boolean)
    max_size: int = Column(BigInteger)
    max_objects: int = Column(Integer)

    exec_time: S3ExecTime = relationship("S3ExecTime", foreign_keys=[id_s3_exec_time])
    user: 'S3User' = relationship("S3User", foreign_keys=[S3User.id_s3_quota_user])
    user_bucket: 'S3User' = relationship("S3User", foreign_keys=[S3User.id_s3_quota_bucket])
    bucket: 'S3Bucket' = relationship("S3Bucket", foreign_keys=[S3Bucket.id_s3_quota])

    def update_from_source(self, source: object) -> None:
        super().update_from_source(source)
        self.enabled = source['enabled']
        self.check_on_raw = source['check_on_raw']
        self.max_size = source['max_size']
        self.max_objects = source['max_objects']
