from inspect import currentframe, getframeinfo


class VALKException(Exception):

    def __init__(self, *args: object, **kwargs) -> None:
        super().__init__(*args)
        if kwargs and 'frame' in kwargs.keys():
            frameinfo = getframeinfo(kwargs['frame'])
            self.args += (frameinfo.filename,)
            self.args += (frameinfo.lineno,)
            self.args += (frameinfo.function,)
