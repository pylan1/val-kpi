from sqlalchemy.orm.session import Session
from rgwadmin import RGWAdmin
from traitlets import Unicode

from val_kpi import sub_command_registration, l, time_fun
from val_kpi.bd import VALKDatabase
from val_kpi.valk import VALKConf
from val_kpi.model.s3 import (
    S3User,
    S3Bucket,
    S3Usage,
    S3Quota,
)


class S3Stats(VALKConf):
    endpoint = Unicode("", help="URL du serveur s3").tag(config=True)
    path = Unicode("", help="Chemin pour l'adminops").tag(config=True)
    access_key = Unicode("", help="L'access_key").tag(config=True)
    secret_key = Unicode("", help="Le secret_key").tag(config=True)

    @staticmethod
    def get_rados_client(endpoint, access_key, secret_key, path):
        rgw = RGWAdmin(
            server=endpoint,
            access_key=access_key,
            secret_key=secret_key,
            admin=path,
        )
        return rgw

    @staticmethod
    def get_pg_session():
        vd = VALKDatabase()
        return vd.get_session()

    # @staticmethod
    @time_fun
    def get_s3_users(self, rgw: RGWAdmin):
        users = {}
        user_ids = rgw.get_users()
        for user_id in user_ids:
            users[user_id] = rgw.get_user(user_id)
        return users

    @time_fun
    def get_s3_buckets(self, rgw: RGWAdmin):
        user_bucket = {}
        buckets = rgw.get_bucket(stats=True)
        for bucket in buckets:
            user_id = bucket["owner"]
            if user_id not in user_bucket.keys():
                user_bucket[user_id] = []
            user_bucket[user_id] += [bucket]
        return user_bucket

    def fetch_and_update_user(self, session: Session, s3_user: dict):
        user_id = s3_user['user_id']
        pg_user = session.query(S3User).filter_by(user_id=user_id).first()
        if not pg_user:
            pg_user = S3User()
            session.add(pg_user)
        pg_user.update_from_source(s3_user)

        if not pg_user.user_quota:
            pg_user.user_quota = S3Quota()
        pg_user.user_quota.update_from_source(s3_user['user_quota'])

        if not pg_user.bucket_quota:
            pg_user.bucket_quota = S3Quota()
        pg_user.bucket_quota.update_from_source(s3_user['bucket_quota'])
        return pg_user

    def get_user_bucket_ids(self, pg_user: S3User, buckets_from_s3: dict):
        s3_user_id = pg_user.user_id

        bucket_id_in_pg = []
        bucket_id_in_rados = []
        for bucket in buckets_from_s3[s3_user_id]:
            bucket_id_in_rados += [bucket['id']]
        for bucket in pg_user.buckets:
            bucket_id_in_pg += [bucket.bucket_id]

        return bucket_id_in_pg, bucket_id_in_rados

    def update_bucket_child(self, pg_bucket: S3Bucket, user_s3_bucket: dict):

        if 'bucket_quota' in user_s3_bucket.keys():
            bucket_quota = user_s3_bucket['bucket_quota']
            if not pg_bucket.quota:
                pg_bucket.quota = S3Quota()
            pg_bucket.quota.update_from_source(bucket_quota)
        elif pg_bucket.quota:
            pg_bucket.quota.delete()

        s3_usage = user_s3_bucket['usage']
        if s3_usage and 'rgw.main' in s3_usage.keys():
            usage = S3Usage()
            pg_bucket.usages.add(usage)
            usage.update_from_source(s3_usage['rgw.main'])

    def update_user_buckets(self, session: Session, pg_user: S3User, buckets_from_s3: dict):
        s3_user_id = pg_user.user_id
        user_s3_buckets = {}
        for bucket in buckets_from_s3[s3_user_id]:
            user_s3_buckets[bucket['id']] = bucket

        bucket_id_in_pg, bucket_id_in_s3 = self.get_user_bucket_ids(pg_user, buckets_from_s3)
        buckets_intersection = set(bucket_id_in_pg) & set(bucket_id_in_s3)
        buckets_only_in_s3 = set(bucket_id_in_s3) - set(bucket_id_in_pg)
        buckets_only_in_pg = set(bucket_id_in_pg) - set(bucket_id_in_s3)

        # Ajout des nouveaux éléments
        for bucket_id in buckets_only_in_s3:
            l.debug(f"Ajout du bucket {s3_user_id}/{bucket_id}")
            pg_bucket = S3Bucket()
            pg_user.buckets.add(pg_bucket)
            pg_bucket.update_from_source(user_s3_buckets[bucket_id])
            self.update_bucket_child(pg_bucket, user_s3_buckets[bucket_id])

        # Mise à jour des buckets existants des 2 côtés
        for bucket_id in buckets_intersection:
            for pg_bucket in session.query(S3Bucket).filter_by(bucket_id=bucket_id):
                if pg_bucket.bucket_id == bucket_id:
                    l.debug(f"Mise à jour du bucket {s3_user_id}/{pg_bucket.bucket_id}")
                    pg_bucket.update_from_source(user_s3_buckets[pg_bucket.bucket_id])
                    self.update_bucket_child(pg_bucket, user_s3_buckets[pg_bucket.bucket_id])

        for bucket_id in buckets_only_in_pg:
            for pg_bucket in session.query(S3Bucket).filter_by(bucket_id=bucket_id):
                if pg_bucket.bucket_id == bucket_id:
                    l.info(f"Suppression d'un pg_bucket zombie: {s3_user_id}/{pg_bucket.name}")
                    session.delete(pg_bucket)

    @time_fun
    def main(self):
        l.debug("[S3] Connexion au rados")
        rgw = self.get_rados_client(self.endpoint,
                                    self.access_key,
                                    self.secret_key,
                                    self.path)
        l.debug("[S3] Connexion à la BD PG")
        session = self.get_pg_session()

        l.debug("[S3] Obtention des utilisateurs avec rados")
        users_from_s3 = self.get_s3_users(rgw)

        l.debug("[S3] Obtention des buckets avec rados")
        buckets_from_s3 = self.get_s3_buckets(rgw)

        l.debug("[S3] Mise à jour des utilisateurs vers la BD")
        for s3_user_id, s3_user in users_from_s3.items():
            pg_user = self.fetch_and_update_user(session, s3_user)
            if s3_user_id in buckets_from_s3.keys():
                self.update_user_buckets(session, pg_user, buckets_from_s3)

        l.debug("[S3] commit vers la BD PG")
        session.commit()
        session.close()


sub_command_registration += [{
    "name": "s3",
    "class": "val_kpi.commands.s3.S3Stats",
}]
