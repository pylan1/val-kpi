"""
Module vap_kpi contient l'outil en ligne de commande pour extraire les KPI VALERIA
"""
__version__ = "0.1.0"

from datetime import datetime
from pathlib import Path

from plumbum import cli, colors
from traitlets.config import Configurable
from traitlets.config.loader import load_pyconfig_files

from val_kpi.utils import lpl, fullname

lpl.init_logger("VALK")
l = lpl.get_logger()

sub_command_registration = []


class VALKConf(cli.Application, Configurable):
    VERSION = __version__
    COLOR_MANDATORY = colors.bold & colors.yellow

    def __init__(self, *args):
        super().__init__(*args)
        conf = None
        if hasattr(self, "config_file"):
            conf = self.config_file
        elif hasattr(self, "parent"):
            if hasattr(self.parent, "config_file"):
                conf = self.parent.config_file
        if conf:
            path = Path(conf)
            self.config = load_pyconfig_files([str(path.name)], str(path.parent))


def time_fun(func):
    def time_it(*args, **kwargs):
        the_beginning = datetime.now()
        ret = func(*args, **kwargs)
        the_end = datetime.now()
        l.info(f"Temps d'exécution de {func.__qualname__}: {the_end - the_beginning}")
        return ret

    return time_it


import val_kpi.commands
import val_kpi.bd