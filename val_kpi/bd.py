from plumbum import cli
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from traitlets import Unicode, MetaHasTraits
from traitlets.config import Configurable

from val_kpi.model.base import meta


class Singleton(MetaHasTraits):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class VALKDatabase(Configurable, metaclass=Singleton):
    protocol = Unicode("", help="Protocol de la BD. Il s'agit du premier "
                                "élément de la chaîne de connexion avant les '//:'. "
                                "ex: postgresql").tag(config=True)
    user = Unicode("", help="Nom de l'utilisateur BD").tag(config=True)
    password = Unicode("", help="Mot de passe de l'utilisateur BD").tag(config=True)
    host = Unicode("", help="URL du serveur BD").tag(config=True)
    port = Unicode("", help="Port de la BD").tag(config=True)
    database = Unicode("", help="Nom de la BD").tag(config=True)

    create_tables = cli.Flag(["--create-tables"], default=False,
                             help="Crée le modèle de données dans la base de données")

    engine = None
    session = None
    meta = None

    def __init__(self, *args):
        super().__init__(*args)

    def __del__(self):
        if self.session:
            self.session.close()

    def get_engine(self):
        if not self.engine:
            self.engine = create_engine(
                f'{self.protocol}://{self.user}:{self.password}@{self.host}:{self.port}/{self.database}',
                connect_args={'options': '-csearch_path={}'.format('stats')})
        return self.engine

    def get_session(self):
        if not self.session:
            SessionClass = sessionmaker(bind=self.get_engine())
            self.session = SessionClass()
        return self.session

    def create_table(self):
        engine = self.get_engine()
        meta.create_all(engine)
