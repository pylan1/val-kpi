"""
VALK est le point d'entrée pour l'interface en ligne de commande
"""
import logging
import os
import stat
from pathlib import Path

from plumbum import cli
from traitlets.config.loader import load_pyconfig_files

import val_kpi.utils as utils
from val_kpi import VALKConf, sub_command_registration, l, time_fun
from val_kpi.bd import VALKDatabase


class VALK(VALKConf):
    DESCRIPTION = "Outil en ligne de commande pour extraire les KPI de VALERIA"

    config_file = cli.SwitchAttr("--config", cli.ExistingFile,
                                 help="Chemin et nom du fichier de configuration (traitlets).",
                                 default=f"{Path.home()}/.config/valk/valk_config.py")

    ignore_security = cli.Flag(["--no-security"], default=False,
                               help="Ignore les erreurs de sécurité du dossier config.")

    verbosity_level = cli.CountOf("-v",
                                  help="Ajuster le niveau de verbosité en ajustant plusieurs v. "
                                       "ex: debug -vvv")

    create_table = cli.Flag(["--create-table"], default=False,
                            help="Crée le modèle de données dans la base de données")

    def init_database(self):
        path = Path(self.config_file)
        vd = VALKDatabase()
        vd.update_config(load_pyconfig_files([str(path.name)], str(path.parent)))


    @staticmethod
    def check_permissions(mode):
        secure = True
        secure &= not bool(mode & stat.S_IROTH)
        secure &= not bool(mode & stat.S_IWOTH)
        secure &= not bool(mode & stat.S_IXOTH)

        secure &= not bool(mode & stat.S_IRGRP)
        secure &= not bool(mode & stat.S_IWGRP)
        secure &= not bool(mode & stat.S_IXGRP)
        return secure

    def validate_security(self):
        is_security_ok = True

        path_info = Path(self.config_file)
        file_mode = os.stat(self.config_file).st_mode
        parent_mode = os.stat(str(path_info.parent)).st_mode

        is_security_ok &= stat.S_ISREG(file_mode)
        is_security_ok &= self.check_permissions(file_mode)
        is_security_ok &= self.check_permissions(parent_mode)

        if not is_security_ok:
            if self.ignore_security:
                l.warning("Problème de sécurité d'accès au fichier ignoré")
            else:
                l.error("La sécurité d'accès au fichier config n'est pas bonne. "
                        "Le groupe et les autres ne doivent pas avoir les droits rwx au fichier "
                        "ou au dossier parent. Au revoir.")
                exit(1)

    def main(self, *args):
        if self.verbosity_level >= 2:
            l.setLevel(logging.DEBUG)
        elif self.verbosity_level >= 1:
            l.setLevel(logging.INFO)
        elif self.verbosity_level >= 1:
            l.setLevel(logging.WARNING)
        else:
            l.setLevel(logging.ERROR)

        self.init_database()
        self.validate_security()

        if self.create_table:
            vd = VALKDatabase()
            vd.create_table()


@time_fun
def main():
    try:
        for sub in sub_command_registration:
            VALK.subcommand(sub["name"], sub["class"])
        VALK.run()
    except Exception as e:
        l.error(f"Woupelaï l'erreur suprême a sonné! Exception: {utils.fullname(e)} >>> {str(e)} >>> {e.__context__}")
        raise e


if __name__ == "__main__":
    main()
