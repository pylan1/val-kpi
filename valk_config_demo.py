c = get_config()

c.S3Stats.endpoint = "s3.valeria.science"
c.S3Stats.path = ""
c.S3Stats.access_key = ""
c.S3Stats.secret_key = ""

c.VALKDatabase.protocol = "postgresql"
c.VALKDatabase.user = ""
c.VALKDatabase.password = ""
c.VALKDatabase.host = ""
c.VALKDatabase.port = "5432"
c.VALKDatabase.database = ""
